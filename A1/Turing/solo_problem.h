#pragma once

#include "problem.h"
#include <chrono>
#include <vector>
#include "mind.h"

using namespace std;
using namespace chrono;

class SoloProblem : public Problem
{
public:
	SoloProblem(const milliseconds& observationPeriod = milliseconds(100), size_t maxObservations = 100);
	virtual vector<float> Solve(vector<Mind>& population) override;

private:
	const milliseconds observationPeriod;
	const size_t maxObservations;

	virtual float Test(Mind& mind) = 0;
};

