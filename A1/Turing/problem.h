#pragma once

#include <vector>
#include "mind.h"

using namespace std;

class Problem
{
public:
	/*
	 * EACH IMPLEMENTOR MUST GUARANTEE THAT THE RETURN VECTOR IS THE SAME
	 * SIZE AS THE VECTOR PARAMETER.
	 */
	virtual vector<float> Solve(vector<Mind>& population) = 0;
};
