#include "opcodes.h"
#include <fstream>
#include <sstream>
#include <vector>
#include <limits>

using namespace std;

Opcodes::OpcodeTable Opcodes::opcodes = Opcodes::OpcodeTable();
bool Opcodes::initialized = false;

Opcodes::BadFile::BadFile(const string& errorMessage) : runtime_error(errorMessage) {}

void Opcodes::Initialize(const string& filename)
{
	ifstream file(filename, ios::in);
	if (!file.is_open())
	{
		throw BadFile("Could not find " + filename + ".");
	}

	size_t fileLineNumber;

	string line;
	getline(file, line);
	fileLineNumber = 1;
	if (line != "\"opcode\",\"name\",\"args...\"")
	{
		throw BadFile("Column headers incorrect. Line: " + to_string(fileLineNumber));
	}
	fileLineNumber++;

	stringstream record;
	string opcodeString;
	OpcodeType opcode;
	string name;
	string arg;
	vector<string> args;
	while (getline(file, line))
	{
		if (line.empty())
		{
			// newline at end of file
			break;
		}
		record.str(line);
		record.seekg(0);

		// read + check opcode
		getline(record, opcodeString, ',');
		if (opcodeString.empty() || opcodeString.front() != '\"' || opcodeString.back() != '\"')
		{
			throw BadFile("Error in opcode. Line: " + to_string(fileLineNumber));
		}
		try
		{
			opcode = (OpcodeType) stoull(opcodeString.substr(1, opcodeString.length() - 2), nullptr, 0);
		}
		catch (invalid_argument)
		{
			throw BadFile("Error in opcode. Line: " + to_string(fileLineNumber));
		}
		catch (out_of_range)
		{
			throw BadFile("Error in opcode. Line: " + to_string(fileLineNumber));
		}
		
		// read + check name
		getline(record, name, ',');
		if (name.empty() || name.front() != '\"' || name.back() != '\"')
		{
			throw BadFile("Error in opcode name. Line: " + to_string(fileLineNumber));
		}

		// read + check args
		args.clear();
		while (!record.eof())
		{
			getline(record, arg, ',');
			if (arg.empty() || arg.front() != '\"' || arg.back() != '\"')
			{
				throw BadFile("Error in opcode arguments. Line: " + to_string(fileLineNumber));
			}
			args.push_back(arg.substr(1, arg.length() - 2));
		}
		if (args.size() > numeric_limits<ArgsSizeType>::max())
		{
			throw BadFile("Error in opcode arguments. Line: " + to_string(fileLineNumber));
		}

		opcodes.emplace(opcode, make_pair(name.substr(1, name.length() - 2), (unsigned char) args.size()));
	
		fileLineNumber++;
	}

	initialized = true;
}

bool Opcodes::IsInitialized() noexcept
{
	return initialized;
}

size_t Opcodes::OpcodesSize() noexcept
{
	return opcodes.size();
}

pair<string, Opcodes::ArgsSizeType> Opcodes::Operation(OpcodeType opcode)
{
	return opcodes.at(opcode);
}
