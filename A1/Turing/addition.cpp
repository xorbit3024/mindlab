#include "addition.h"
#include <map>
#include <future>
#include <thread>
#include <limits>

#ifdef _DEBUG
#include <iostream>
#endif // _DEBUG

using namespace std;
using namespace chrono;

Addition::Addition(const milliseconds& observationPeriod, size_t maxObservations) :
    SoloProblem(observationPeriod, maxObservations), rng(random_device()()) {}

float Addition::Test(Mind& mind)
{
    float sum = 0.0f;

    for (uint8_t counter = 0; counter < 30; counter++)
    {
        try
        {
            Mind::DataType a = (Mind::DataType) (rng() % (numeric_limits<Mind::DataType>::max() + 1));
            Mind::DataType b = (Mind::DataType) (rng() % (numeric_limits<Mind::DataType>::max() + 1));

            if (numeric_limits<Mind::DataType>::max() - a < b)
            {
                counter--;
                continue;
            }

            Mind::ChromosomeType result = mind.Observe({ a, b });

            if (!result.empty())
            {
                sum += (1.0f - (abs(((float) a + b)  - result.front()) / (a + b)));
            }
        }
        catch (Mind::BadChromosome&) {}
    }

    return sum / 30.0f;
}
