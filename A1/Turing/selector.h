#pragma once

#include <stdexcept>
#include <string>
#include <vector>
#include "mind.h"
#include "problem.h"
#include <random>

using namespace std;

class Selector
{
public:
	class InitialParametersError : public runtime_error
	{
	public:
		InitialParametersError(const string& errorMessageArgument = "Error in initial parameters.");
	};

	Selector() = delete;
	Selector
	(
		float targetFitness = 1.0f,
		vector<Mind::ChromosomeType>&& chromosomes = {},
		size_t populationSize = 30,
		size_t survivors = 5,
		float mutationStrength = 1.0f,
		float nonChampionReproductionStrength = 0.5f,
		float insertionRate = 0.1f,
		float deletionRate = 0.1f,
		float pointRate = 0.5f
	);

	Mind Select(Problem& problem);
private:
	const size_t initialPopulationSize;
	const vector<Mind::ChromosomeType> initialChromosomes;
	float insertionRate;
	float deletionRate;
	float pointRate;
	float mutationStrength;
	size_t survivors;
	const float targetFitness;
	float nonChampionReproductionStrength;
	
	minstd_rand standardRNG;

	float RNG();
	Mind::ChromosomeType MutateMultiple(const vector<Mind::ChromosomeType>& parents);
	Mind::DataType PointMutate(Mind::DataType parentData);
	
	class FiniteRatioDistribution
	{
	public:
		FiniteRatioDistribution(float r, size_t n);

		size_t operator()();
	private:
		float r;
		size_t n;

		minstd_rand distributionStandardRNG;

		float RandomProbability();
	};
};
