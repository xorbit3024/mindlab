#include "mind.h"
#include <fstream>

using namespace std;

Mind::BadChromosome::BadChromosome(const string& errorMessage) : runtime_error(errorMessage) {}
Mind::BadFile::BadFile(const string& errorMessage) : runtime_error(errorMessage) {}

Mind::Mind(ChromosomeType&& chromosome) : chromosome(forward<ChromosomeType>(chromosome))
{
    if (!Opcodes::IsInitialized())
    {
        Opcodes::Initialize();
    }
}

Mind::Mind(const string& fileName)
{
    ifstream file(fileName, ios::in | ios::binary);

    if (!file.is_open())
    {
        throw BadFile("Chromosome file not found.");
    }

    uint8_t dataSize;
    file.read((char*) (&dataSize), sizeof(dataSize));

    if (dataSize > sizeof(DataType))
    {
        throw BadFile("Chromosome file data size too large.");
    }

    DataType data = 0;
    while (file.read((char*) (&data), dataSize))
    {
        chromosome.push_back(data);
        data = 0;
    }
}

void Mind::Save(const string& fileName) const
{
    ofstream file(fileName, ios::out | ios::binary);

    if (!file.is_open())
    {
        throw BadFile("Unable to create chromosome file.");
    }

    uint8_t dataSize = sizeof(DataType);
    file.write((char*) (&dataSize), sizeof(dataSize));

    for (DataType data : chromosome)
    {
        file.write((char*) (&data), sizeof(data));
    }
}

vector<Mind::DataType> Mind::Observe(vector<Mind::DataType>&& input)
{
    I = forward<vector<Mind::DataType>>(input);
    O.clear();

    size_t readPosition = 0;
    Operation currentOperation;

    while (readPosition < chromosome.size())
    {
        currentOperation = ModOpcode(chromosome.at(readPosition));

        if (chromosome.size() < currentOperation.second + readPosition + 1)
        {
            throw BadChromosome("Not enough arguments. Opcode position: " + to_string(readPosition));
        }

        if (currentOperation.first == "RETURN")
        {
            return O;
        }
        else if (currentOperation.first == "GOTO")
        {
            readPosition = chromosome.at(readPosition + 1);
        }
        else if (currentOperation.first.substr(0, 2) == "IF")
        {
            DataType left = 0;
            DataType right = 0;

            switch (currentOperation.first.at(currentOperation.first.size() - 2))
            {
            case 'L':
                left = chromosome.at(readPosition + 1);
                break;
            case 'I':
                CheckI(chromosome.at(readPosition + 1), readPosition);
                left = I.at(chromosome.at(readPosition + 1));
                break;
            case 'P':
                ResizeP(chromosome.at(readPosition + 1), readPosition);
                left = P.at(chromosome.at(readPosition + 1));
                break;
            }
            switch (currentOperation.first.at(currentOperation.first.size() - 1))
            {
            case 'L':
                right = chromosome.at(readPosition + 2);
                break;
            case 'I':
                CheckI(chromosome.at(readPosition + 2), readPosition);
                right = I.at(chromosome.at(readPosition + 2));
                break;
            case 'P':
                ResizeP(chromosome.at(readPosition + 2), readPosition);
                right = P.at(chromosome.at(readPosition + 2));
                break;
            }

            if (currentOperation.first.at(2) == 'N')
            {
                if (currentOperation.first.at(3) == 'G')
                {
                    if (!(left > right))
                    {
                        readPosition += currentOperation.second;
                    }
                    else
                    {
                        readPosition += (size_t) currentOperation.second + 1 +
                            ModOpcode(chromosome.at(readPosition + currentOperation.second)).second;
                    }
                }
                else if (currentOperation.first.at(3) == 'L')
                {
                    if (!(left < right))
                    {
                        readPosition += currentOperation.second;
                    }
                    else
                    {
                        readPosition += (size_t) currentOperation.second + 1 +
                            ModOpcode(chromosome.at(readPosition + currentOperation.second)).second;
                    }
                }
                else if (currentOperation.first.at(3) == 'E')
                {
                    if (left != right)
                    {
                        readPosition += currentOperation.second;
                    }
                    else
                    {
                        readPosition += (size_t) currentOperation.second + 1 +
                            ModOpcode(chromosome.at(readPosition + currentOperation.second)).second;
                    }
                }
            }
            else
            {
                if (currentOperation.first.at(2) == 'G')
                {
                    if (left > right)
                    {
                        readPosition += currentOperation.second;
                    }
                    else
                    {
                        readPosition += (size_t) currentOperation.second + 1 +
                            ModOpcode(chromosome.at(readPosition + currentOperation.second)).second;
                    }
                }
                else if (currentOperation.first.at(2) == 'L')
                {
                    if (left < right)
                    {
                        readPosition += currentOperation.second;
                    }
                    else
                    {
                        readPosition += (size_t) currentOperation.second + 1 +
                            ModOpcode(chromosome.at(readPosition + currentOperation.second)).second;
                    }
                }
                else if (currentOperation.first.at(2) == 'E')
                {
                    if (left == right)
                    {
                        readPosition += currentOperation.second;
                    }
                    else
                    {
                        readPosition += (size_t) currentOperation.second + 1 +
                            ModOpcode(chromosome.at(readPosition + currentOperation.second)).second;
                    }
                }
            }
        }
        else if (currentOperation.first.substr(0, 3) == "SET")
        {
            DataType* left = nullptr;
            DataType right = 0;

            switch (currentOperation.first.at(3))
            {
            case 'P':
                ResizeP(chromosome.at(readPosition + 1), readPosition);
                left = &(P.at(chromosome.at(readPosition + 1)));
                break;
            case 'O':
                ResizeO(chromosome.at(readPosition + 1), readPosition);
                left = &(O.at(chromosome.at(readPosition + 1)));
                break;
            }
            switch (currentOperation.first.at(4))
            {
            case 'L':
                right = chromosome.at(readPosition + 2);
                break;
            case 'I':
                if (currentOperation.first.back() == 'S')
                {
                    right = (DataType) I.size();
                }
                else
                {
                    CheckI(chromosome.at(readPosition + 2), readPosition);
                    right = I.at(chromosome.at(readPosition + 2));
                }
                break;
            case 'P':
                if (currentOperation.first.back() == 'S')
                {
                    right = (DataType) P.size();
                }
                else
                {
                    ResizeP(chromosome.at(readPosition + 2), readPosition);
                    right = P.at(chromosome.at(readPosition + 2));
                }
                break;
            case 'O':
                if (currentOperation.first.back() == 'S')
                {
                    right = (DataType) O.size();
                }
                else
                {
                    CheckO(chromosome.at(readPosition + 2), readPosition);
                    right = O.at(chromosome.at(readPosition + 2));
                }
                break;
            case 'C':
                if (currentOperation.first.back() == 'S')
                {
                    right = (DataType) chromosome.size();
                }
                else
                {
                    CheckC(chromosome.at(readPosition + 2), readPosition);
                    right = chromosome.at(chromosome.at(readPosition + 2));
                }
                break;
            }

            *left = right;

            readPosition += (size_t) currentOperation.second + 1;
        }
        else if (currentOperation.first.substr(0, 3) == "SUM")
        {
            DataType* sum = nullptr;
            DataType left = 0;
            DataType right = 0;

            switch (currentOperation.first.at(3))
            {
            case 'P':
                ResizeP(chromosome.at(readPosition + 1), readPosition);
                sum = &(P.at(chromosome.at(readPosition + 1)));
                break;
            case 'O':
                ResizeO(chromosome.at(readPosition + 1), readPosition);
                sum = &(O.at(chromosome.at(readPosition + 1)));
                break;
            }
            switch (currentOperation.first.at(4))
            {
            case 'L':
                left = chromosome.at(readPosition + 2);
                break;
            case 'I':
                CheckI(chromosome.at(readPosition + 2), readPosition);
                left = I.at(chromosome.at(readPosition + 2));
                break;
            case 'P':
                ResizeP(chromosome.at(readPosition + 2), readPosition);
                left = P.at(chromosome.at(readPosition + 2));
                break;
            }
            switch (currentOperation.first.at(5))
            {
            case 'L':
                right = chromosome.at(readPosition + 3);
                break;
            case 'I':
                CheckI(chromosome.at(readPosition + 3), readPosition);
                right = I.at(chromosome.at(readPosition + 3));
                break;
            case 'P':
                ResizeP(chromosome.at(readPosition + 3), readPosition);
                right = P.at(chromosome.at(readPosition + 3));
                break;
            }

            *sum = left + right;

            readPosition += (size_t) currentOperation.second + 1;
        }
        else if (currentOperation.first.substr(0, 3) == "DIF")
        {
            DataType* difference = nullptr;
            DataType left = 0;
            DataType right = 0;

            switch (currentOperation.first.at(3))
            {
            case 'P':
                ResizeP(chromosome.at(readPosition + 1), readPosition);
                difference = &(P.at(chromosome.at(readPosition + 1)));
                break;
            case 'O':
                ResizeO(chromosome.at(readPosition + 1), readPosition);
                difference = &(O.at(chromosome.at(readPosition + 1)));
                break;
            }
            switch (currentOperation.first.at(4))
            {
            case 'L':
                left = chromosome.at(readPosition + 2);
                break;
            case 'I':
                CheckI(chromosome.at(readPosition + 2), readPosition);
                left = I.at(chromosome.at(readPosition + 2));
                break;
            case 'P':
                ResizeP(chromosome.at(readPosition + 2), readPosition);
                left = P.at(chromosome.at(readPosition + 2));
                break;
            }
            switch (currentOperation.first.at(5))
            {
            case 'L':
                right = chromosome.at(readPosition + 3);
                break;
            case 'I':
                CheckI(chromosome.at(readPosition + 3), readPosition);
                right = I.at(chromosome.at(readPosition + 3));
                break;
            case 'P':
                ResizeP(chromosome.at(readPosition + 3), readPosition);
                right = P.at(chromosome.at(readPosition + 3));
                break;
            }

            *difference = left - right;

            readPosition += (size_t) currentOperation.second + 1;
        }
        else if (currentOperation.first.substr(0, 3) == "INS")
        {
            if (chromosome.at(readPosition + 1) > chromosome.size())
            {
                throw BadChromosome("Cannot insert past end of chromsome. Opcode position: " + to_string(readPosition));
            }

            DataType position = chromosome.at(readPosition + 1);
            DataType gene = DataType();

            switch (currentOperation.first.at(3))
            {
            case 'L':
                gene = chromosome.at(readPosition + 2);
                break;
            case 'I':
                CheckI(chromosome.at(readPosition + 2), readPosition);
                gene = I.at(chromosome.at(readPosition + 2));
                break;
            case 'P':
                ResizeP(chromosome.at(readPosition + 2), readPosition);
                gene = P.at(chromosome.at(readPosition + 2));
                break;
            }

            chromosome.insert(chromosome.begin() + position, gene);

            if (position <= readPosition + currentOperation.second)
            {
                readPosition += (size_t) currentOperation.second + 2;
            }
            else
            {
                readPosition += (size_t) currentOperation.second + 1;
            }
        }
        else if (currentOperation.first.substr(0, 3) == "DEL")
        {
            if (chromosome.at(readPosition + 1) >= chromosome.size())
            {
                throw BadChromosome("Cannot delete outside of chromsome. Opcode position: " + to_string(readPosition));
            }

            DataType position = chromosome.at(readPosition + 1);

            chromosome.erase(chromosome.begin() + position);

            if (position <= readPosition + currentOperation.second)
            {
                readPosition += currentOperation.second;
            }
            else
            {
                readPosition += (size_t) currentOperation.second + 1;
            }
        }
        else if (currentOperation.first.substr(0, 3) == "REP")
        {
            if (chromosome.at(readPosition + 1) >= chromosome.size())
            {
                throw BadChromosome("Cannot replace outside of chromsome. Opcode position: " + to_string(readPosition));
            }

            DataType gene = DataType();

            switch (currentOperation.first.at(3))
            {
            case 'L':
                gene = chromosome.at(readPosition + 2);
                break;
            case 'I':
                CheckI(chromosome.at(readPosition + 2), readPosition);
                gene = I.at(chromosome.at(readPosition + 2));
                break;
            case 'P':
                ResizeP(chromosome.at(readPosition + 2), readPosition);
                gene = P.at(chromosome.at(readPosition + 2));
                break;
            }

            chromosome.at(chromosome.at(readPosition + 1)) = gene;

            readPosition += (size_t) currentOperation.second + 1;
        }
    }

    return O;
}

Mind::ChromosomeType Mind::Chromosome() const
{
    return chromosome;
}

Mind::Operation Mind::ModOpcode(Opcodes::OpcodeType opcode)
{
    return Opcodes::Operation(opcode % Opcodes::OpcodesSize());
}

void Mind::CheckI(size_t index, size_t readPosition)
{
    if (I.size() <= index)
    {
        throw BadChromosome("Input out of range. Opcode position: " + to_string(readPosition));
    }
}

void Mind::CheckO(size_t index, size_t readPosition)
{
    if (O.size() <= index)
    {
        throw BadChromosome("Output out of range. Opcode position: " + to_string(readPosition));
    }
}

void Mind::CheckC(size_t index, size_t readPosition)
{
    if (chromosome.size() <= index)
    {
        throw BadChromosome("Chromosome out of range. Opcode position: " + to_string(readPosition));
    }
}

void Mind::ResizeP(size_t index, size_t readPosition)
{
    if (index >= P.max_size())
    {
        throw BadChromosome("Target index too high. Opcode position: " + to_string(readPosition));
    }

    if (P.size() <= index)
    {
        P.resize(index + 1, 0);
    }
}

void Mind::ResizeO(size_t index, size_t readPosition)
{
    if (index >= O.max_size())
    {
        throw BadChromosome("Target index too high. Opcode position: " + to_string(readPosition));
    }

    if (O.size() <= index)
    {
        O.resize(index + 1, 0);
    }
}

ostream& operator<<(ostream& stream, const Mind::ChromosomeType& chromosome)
{
    stream << "{ ";
    for (Mind::ChromosomeType::const_iterator gene = chromosome.cbegin(); gene < chromosome.cend(); gene++)
    {
        stream << (uint64_t) *gene;
        if ((gene + 1) < chromosome.cend())
        {
            stream << ", ";
        }
    }
    stream << " }";

    return stream;
}
