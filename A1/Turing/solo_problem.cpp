#include "solo_problem.h"
#include <map>
#include <future>
#include <thread>

#ifdef _DEBUG
#include <iostream>
#endif // _DEBUG


using namespace std;
using namespace chrono;

SoloProblem::SoloProblem(const milliseconds& observationPeriod, size_t maxObservations) :
    observationPeriod(observationPeriod), maxObservations(maxObservations) {}

vector<float> SoloProblem::Solve(vector<Mind>& population)
{
    map<size_t, future<float>> futureFitnesses;
    for (size_t individualIndex = 0; individualIndex < population.size(); individualIndex++)
    {
        packaged_task<float(Mind& mind)> task(bind(&SoloProblem::Test, this, placeholders::_1));
        futureFitnesses.emplace_hint(futureFitnesses.end(), individualIndex, task.get_future());
        thread(move(task), ref(population[individualIndex])).detach();
    }

    vector<float> fitnesses(population.size(), 0.0f);

    for (size_t observations = 0; futureFitnesses.size() > 0 && observations < maxObservations; observations++)
    {
        this_thread::sleep_for(observationPeriod);

        vector<map<size_t, future<float>>::iterator> finishedIndividuals;

        for (map<size_t, future<float>>::iterator individual = futureFitnesses.begin();
            individual != futureFitnesses.end(); individual++)
        {
            if (individual->second.wait_for(milliseconds::zero()) == future_status::ready)
            {
                finishedIndividuals.emplace_back(individual);
                fitnesses[individual->first] = individual->second.get();
            }
        }

        for (vector<map<size_t, future<float>>::iterator>::const_iterator finishedIndividual = finishedIndividuals.cbegin();
            finishedIndividual < finishedIndividuals.cend(); finishedIndividual++)
        {
            futureFitnesses.erase(*finishedIndividual);
        }

#ifdef _DEBUG
        cout << "observations: " << observations + 1 << endl;
        cout << "finished: " << population.size() - futureFitnesses.size() << endl;
#endif // _DEBUG

    }

    return fitnesses;
}
