#pragma once

#include "solo_problem.h"
#include <chrono>
#include <random>

using namespace std;
using namespace chrono;

class Addition : public SoloProblem
{
public:
	Addition(const milliseconds& observationPeriod = milliseconds(1000), size_t maxObservations = 10);

private:
	minstd_rand rng;

	virtual float Test(Mind& mind) override;
};
