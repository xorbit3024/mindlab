#pragma once

#include <stdexcept>
#include <string>
#include <map>

using namespace std;

class Opcodes
{
public:
	class BadFile : public runtime_error
	{
	public:
		BadFile(const string& errorMessage = "Error in opcodes file.");
	};

	static void Initialize(const string& filename = "opcodes.csv");
	static bool IsInitialized() noexcept;
	
	using OpcodeType = uint8_t;
	using ArgsSizeType = uint8_t;

	static size_t OpcodesSize() noexcept;
	static pair<string, ArgsSizeType> Operation(OpcodeType opcode);
private:
	using OpcodeTable = map<OpcodeType, pair<string, ArgsSizeType>>;
	static OpcodeTable opcodes;

	static bool initialized;
};
