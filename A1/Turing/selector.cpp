#define GENERATION_PAUSE

#include "selector.h"
#include <map>

#ifdef GENERATION_PAUSE
#include <iostream>
#endif // GENERATION_PAUSE

using namespace std;

Selector::InitialParametersError::InitialParametersError(const string& errorMessage) : runtime_error(errorMessage) {}

Selector::Selector
(
    float targetFitness,
    vector<Mind::ChromosomeType>&& chromosomes,
    size_t populationSize,
    size_t survivors,
    float mutationStrength,
    float nonChampionReproductionStrength,
    float insertionRate,
    float deletionRate,
    float pointRate
) :
    targetFitness(targetFitness),
    initialChromosomes(forward<vector<Mind::ChromosomeType>>(chromosomes)),
    initialPopulationSize(populationSize)
{
    if (survivors >= populationSize)
    {
        throw InitialParametersError("Must have fewer survivors than population size.");
    }
    this->survivors = survivors;

    if (mutationStrength <= 0.0f)
    {
        throw InitialParametersError("Mutation Strength must be positive.");
    }
    this->mutationStrength = mutationStrength;

    if (nonChampionReproductionStrength < 0.0f || nonChampionReproductionStrength >= 1.0f)
    {
        throw InitialParametersError("Non-champion reproduction strength must be in [0, 1).");
    }
    this->nonChampionReproductionStrength = nonChampionReproductionStrength;

    if (insertionRate < 0.0f || insertionRate > 1.0f)
    {
        throw InitialParametersError("Insertion Rate must be a probability.");
    }
    this->insertionRate = insertionRate;

    if (deletionRate < 0.0f || deletionRate > 1.0f)
    {
        throw InitialParametersError("Deletion Rate must be a probability.");
    }
    this->deletionRate = insertionRate;

    if (pointRate < 0.0f || pointRate > 1.0f)
    {
        throw InitialParametersError("Point Rate must be a probability.");
    }
    this->pointRate = pointRate;

    standardRNG.seed(random_device()());
}

Mind Selector::Select(Problem& problem)
{
    vector<Mind> population;
    size_t index;

    for (index = 0; index < initialPopulationSize; index++)
    {
        population.emplace_back(MutateMultiple(initialChromosomes));
    }

    vector<float> fitnesses = problem.Solve(population);

    multimap<float, Mind, greater<float>> sortedPopulation;
    for (index = 0; index < population.size(); index++)
    {
        sortedPopulation.emplace(fitnesses.at(index), population.at(index));
    }
    while (sortedPopulation.size() > survivors)
    {
        sortedPopulation.erase(--sortedPopulation.end());
    }

#ifdef GENERATION_PAUSE
    cout << sortedPopulation.cbegin()->first << endl;
    cout << sortedPopulation.cbegin()->second.Chromosome() << endl;
    system("pause");
#endif // GENERATION_PAUSE

    while (sortedPopulation.cbegin()->first < targetFitness)
    {
        vector<Mind::ChromosomeType> parentChromosomes;
        for (map<float, Mind, greater<float>>::const_iterator parentIterator = sortedPopulation.cbegin(); parentIterator != sortedPopulation.cend(); parentIterator++)
        {
            parentChromosomes.emplace_back(parentIterator->second.Chromosome());
        }

        population.clear();

        for (index = 0; index < initialPopulationSize; index++)
        {
            population.emplace_back(MutateMultiple(parentChromosomes));
        }

        fitnesses = problem.Solve(population);

        sortedPopulation.clear();
        for (index = 0; index < population.size(); index++)
        {
            sortedPopulation.emplace(fitnesses.at(index), population.at(index));
        }
        while (sortedPopulation.size() > survivors)
        {
            sortedPopulation.erase(--sortedPopulation.end());
        }

#ifdef GENERATION_PAUSE
        cout << sortedPopulation.cbegin()->first << endl;
        cout << sortedPopulation.cbegin()->second.Chromosome() << endl;
        system("pause");
#endif // GENERATION_PAUSE
    }

    return sortedPopulation.cbegin()->second;
}

float Selector::RNG()
{
    return ((float) standardRNG()) / standardRNG.max();
}

Mind::ChromosomeType Selector::MutateMultiple(const vector<Mind::ChromosomeType>& parents)
{
    Mind::ChromosomeType child;

    FiniteRatioDistribution parentSelectDistribution(nonChampionReproductionStrength, parents.size());

    size_t parentIndex = parentSelectDistribution();

    for (size_t chromosomeIndex = 0; chromosomeIndex < parents.at(parentIndex).size(); chromosomeIndex++)
    {
        if (RNG() < insertionRate)
        {
            child.push_back(PointMutate(parents.at(parentIndex).at(chromosomeIndex)));
        }
        if (RNG() < deletionRate)
        {
            continue;
        }
        else if (RNG() < pointRate)
        {
            child.push_back(PointMutate(parents.at(parentIndex).at(chromosomeIndex)));
        }
        else
        {
            child.push_back(parents.at(parentIndex).at(chromosomeIndex));
        }

        parentIndex = parentSelectDistribution();
    }

    return child;
}

Mind::DataType Selector::PointMutate(Mind::DataType parentData)
{
    return (Mind::DataType) round(normal_distribution<float>((float) parentData, mutationStrength)(standardRNG));
}

Selector::FiniteRatioDistribution::FiniteRatioDistribution(float r, size_t n) : r(r), n(n)
{
    distributionStandardRNG.seed(random_device()());
}

size_t Selector::FiniteRatioDistribution::operator()()
{
    float roll = RandomProbability();
    float sum = 0;
    for (size_t guess = 0; guess < n; guess++)
    {
        sum += (float) ((((double) r) - 1) / (pow(r, n) - 1) * pow(r, guess));

        if (roll <= sum)
        {
            return guess;
        }
    }

    throw runtime_error("Calculation error in FiniteRatioDistribution.");
}

float Selector::FiniteRatioDistribution::RandomProbability()
{
    return ((float) distributionStandardRNG()) / distributionStandardRNG.max();
}
