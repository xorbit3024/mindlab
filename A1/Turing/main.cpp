#include <iostream>
#include <cstdlib>
#include "opcodes.h"
#include "mind.h"
#include "selector.h"
#include "addition.h"

using namespace std;

int main()
{
	try
	{
		Opcodes::Initialize("simple.csv");

		Selector s
		(
			0.99f,								// targetFitness

												// chromosomes
			{ { 5, 0, 0,
				5, 1, 1,
				8, 0, 0, 1,
				7, 0, 0 } },

			10,									// populationSize
			2,									// survivors

			1.0f,								// mutationStrength
			0.01f,								// nonChampionReproductionStrength
			0.01f,								// insertionRate
			0.01f,								// deletionRate
			0.25f								// pointRate
		);

		Addition problem;
		auto champion = s.Select(problem);
		cout << champion.Chromosome() << endl;

		/* Written Programs
		 * ------------------------------------------------------------------
		 * Adder: { 0x29, 0x00, 0x00, 0x01 } - opcodes.csv
		 * Adder: { 5, 0, 0,
					5, 1, 1,
					8, 0, 0, 1,
					7, 0, 0 } - simple.csv
		 * 
		 * 
		 * 
		 */
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
	}
	
	system("pause");
	return EXIT_SUCCESS;
}
