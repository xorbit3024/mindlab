#pragma once

#include <stdexcept>
#include "opcodes.h"
#include <vector>
#include <iostream>
#include <string>

using namespace std;

class Mind
{
public:
	class BadChromosome : public runtime_error
	{
	public:
		BadChromosome(const string& errorMessage = "Error in chromosome.");
	};
	class BadFile : public runtime_error
	{
	public:
		BadFile(const string& errorMessage = "Error in chromosome file.");
	};

	using DataType = Opcodes::OpcodeType;
	using ChromosomeType = vector<DataType>;

	Mind() = delete;
	Mind(ChromosomeType&& chromosome);
	Mind(const string& fileName);

	void Save(const string& fileName) const;

	vector<DataType> Observe(vector<DataType>&& input);

	using Operation = pair<string, Opcodes::ArgsSizeType>;
	static Operation ModOpcode(Opcodes::OpcodeType opcode);

	ChromosomeType Chromosome() const;

private:
	ChromosomeType chromosome;
	vector<DataType> I;
	vector<DataType> P;
	vector<DataType> O;

	void CheckI(size_t index, size_t readPosition);
	void CheckO(size_t index, size_t readPosition);
	void CheckC(size_t index, size_t readPosition);
	void ResizeP(size_t index, size_t readPosition);
	void ResizeO(size_t index, size_t readPosition);
};

ostream& operator<<(ostream& stream, const Mind::ChromosomeType& chromosome);
