#include "evolver.h"
#include <vector>

#define DEBUG

#ifdef DEBUG
#include <iostream>
#endif // DEBUG

using namespace std;

Evolver::Evolver()
{
	random_device seeder;
	seed = seeder();
	rng.seed(seed);
}

Mind Evolver::EvolveReflexMind(size_t generations,
	float initialMutationRate,
	size_t populationSize,
	size_t survivorsSize,
	size_t movesPerRound,
	size_t roundsPerGeneration)
{
	_STL_ASSERT(survivorsSize <= populationSize, "Cannot have more survivors than individuals.");

	size_t individualCounter;
	vector<ReflexAgent> population;
	using IndividualPointer = vector<ReflexAgent>::iterator;

	int positionID;
	int parentID;

	vector<short> distances(populationSize, 0);
	float fitness;
	vector<float> highestFitnesses(survivorsSize, -INFINITY);
	size_t fitnessCounter;
	vector<size_t> highestFitnessIndices(survivorsSize, 0);
	constexpr const float e = 2.71828182845904523536028747135266249775724709369995957496696762772407663f;

	using action = vector<bool>;

	vector<Mind::Synapses> parentsSynapses;

	// initialize population
	for (individualCounter = 0; individualCounter < populationSize; individualCounter++)
	{
		Mind::Synapses synapseGraph;
		for (char fromCounter = 0; fromCounter < 4; fromCounter++)
		{
			for (char toCounter = 4; toCounter < 8; toCounter++)
			{
				synapseGraph.insert(make_pair(fromCounter, make_pair(toCounter, static_cast<float>(rng()) / rng.max())));
			}
		}

		positionID = rng() % (4 * movesPerRound);

		population.emplace_back((positionID < (2 * movesPerRound)) ?
				make_pair(min(positionID, (2 * (int)movesPerRound) - positionID), (int)movesPerRound - positionID) :
				make_pair(max((2 * (int)movesPerRound) - positionID, positionID - (4 * (int)movesPerRound)), positionID - (3 * (int)movesPerRound)),
			4, 4, synapseGraph, 0.5f);
	}

	for (size_t generationCounter = 0; generationCounter < generations; generationCounter++)
	{
		distances.assign(populationSize, 0);
		for (size_t roundCounter = 0; roundCounter < roundsPerGeneration; roundCounter++)
		{
			for (char counter = 0; counter < movesPerRound; counter++)
			{
				for (IndividualPointer p = population.begin(); p < population.end(); p++)
				{
					p->mind.Observe({ p->position.first < 0, p->position.second < 0, p->position.first > 0, p->position.second > 0 });
					action a = p->mind.Process(8);

					if (a[0])
					{
						p->position.first++;
					}
					else if (a[1])
					{
						p->position.second++;
					}
					else if (a[2])
					{
						p->position.first--;
					}
					else if (a[3])
					{
						p->position.second--;
					}
				}
			}

			for (individualCounter = 0; individualCounter < populationSize; individualCounter++)
			{
				distances[individualCounter] += (abs(population[individualCounter].position.first) +
												 abs(population[individualCounter].position.second));

				positionID = rng() % (4 * movesPerRound);
				population[individualCounter].position = (positionID < (2 * movesPerRound)) ?
					make_pair(min(positionID, (2 * (int)movesPerRound) - positionID), (int)movesPerRound - positionID) :
					make_pair(max((2 * (int)movesPerRound) - positionID, positionID - (4 * (int)movesPerRound)), positionID - (3 * (int)movesPerRound));
			}
		}

		highestFitnesses.assign(survivorsSize, -INFINITY);
		for (individualCounter = 0; individualCounter < populationSize; individualCounter++)
		{
			fitness = 1.0f / (static_cast<float>(distances[individualCounter]) / roundsPerGeneration);

			for (fitnessCounter = 0; fitnessCounter < survivorsSize; fitnessCounter++)
			{
				if (fitness > highestFitnesses[fitnessCounter])
				{
					for (size_t reverseFitnessCounter = survivorsSize - 1; reverseFitnessCounter > fitnessCounter; reverseFitnessCounter--)
					{
						highestFitnesses[reverseFitnessCounter] = highestFitnesses[reverseFitnessCounter - 1];
						highestFitnessIndices[reverseFitnessCounter] = highestFitnessIndices[reverseFitnessCounter - 1];
					}
					highestFitnesses[fitnessCounter] = fitness;
					highestFitnessIndices[fitnessCounter] = individualCounter;
					break;
				}
			}
		}

#ifdef DEBUG
		for (fitnessCounter = 0; fitnessCounter < survivorsSize; fitnessCounter++)
		{
			cout << highestFitnesses[fitnessCounter] << ", ";
		}
		cout << endl;
#endif // DEBUG

		for (fitnessCounter = 0; fitnessCounter < survivorsSize; fitnessCounter++)
		{
			parentsSynapses.emplace_back(population[highestFitnessIndices[fitnessCounter]].mind.GetSynapses());

			positionID = rng() % (4 * movesPerRound);

			population[fitnessCounter] = ReflexAgent((positionID < (2 * movesPerRound)) ?
				make_pair(min(positionID, (2 * (int)movesPerRound) - positionID), (int)movesPerRound - positionID) :
				make_pair(max((2 * (int)movesPerRound) - positionID, positionID - (4 * (int)movesPerRound)), positionID - (3 * (int)movesPerRound)),
				4, 4, population[highestFitnessIndices[fitnessCounter]].mind.GetSynapses(), 0.5f);
		}

		for (individualCounter = survivorsSize; individualCounter < populationSize; individualCounter++)
		{
			positionID = rng() % (4 * movesPerRound);
			parentID = rng() % survivorsSize;

			population[individualCounter] = ReflexAgent((positionID < (2 * movesPerRound)) ?
				make_pair(min(positionID, (2 * (int)movesPerRound) - positionID), (int)movesPerRound - positionID) :
				make_pair(max((2 * (int)movesPerRound) - positionID, positionID - (4 * (int)movesPerRound)), positionID - (3 * (int)movesPerRound)),
				4, 4, Mutate(population[parentID].mind.GetSynapses(), initialMutationRate * powf(e, -highestFitnesses[0])), 0.5f);
		}
	}

	return population[0].GetMind();
}



Evolver::ReflexAgent::ReflexAgent(
	pair<int, int> initialPosition,
	size_t inputSizeParameter, size_t outputSizeParameter, const Mind::Synapses& synapsesParameter, float thresholdParameter) : 
	position(initialPosition),
	mind(inputSizeParameter, outputSizeParameter, synapsesParameter, thresholdParameter)
{
}

const Mind& Evolver::ReflexAgent::GetMind()
{
	return mind;
}

Mind::Synapses Evolver::Mutate(const Mind::Synapses& parentSynapses, float mutationRate)
{
	Mind::Synapses result;
	using weightIterator = Mind::Synapses::const_iterator;
	for (weightIterator i = parentSynapses.cbegin(); i != parentSynapses.cend(); i++)
	{
		result.insert(make_pair(i->first, make_pair(i->second.first,
			i->second.second * (1.0f + mutationRate * (1.0f - 2.0f * rng() / rng.max())))));
	}
	return result;
}
