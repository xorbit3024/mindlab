#pragma once

#include <vector>
#include <queue>
#include <unordered_map>

using namespace std;

class Mind
{
public:
	using Synapses = unordered_multimap<size_t, pair<size_t, float>>;

	Mind() = delete;
	Mind(
		size_t inputSizeParameter,
		size_t outputSizeParameter,
		const Synapses& synapsesParameter,
		float thresholdParameter,
		float memoryParameter = 0.0f,
		float forgetfulnessParameter = 0.0f,
		float habituationParameter = 1.0f,
		float surpriseParameter = 1.0f);
	Mind(const Mind& other);
	Mind(Mind&& other) noexcept;
	Mind& operator=(const Mind& other);
	Mind& operator=(Mind&& other) noexcept;
	~Mind() = default;
	
	// read input vector<bool> into input neurons
	void Observe(const vector<bool>& input);
	// fire neurons: param of how many neurons to fire
	vector<bool> Process(size_t fires = 1);

	Synapses GetSynapses() const;

private:
	// signal required to fire
	float threshold;
	// signal additive after firing:
	// >0 reinforcement, <0 repression
	float memory;
	// signal additive after not firing:
	// >0 repression, <0 reinforcement
	float forgetfulness;
	// signal multiplier from input after firing:
	// >1 neg feedback, 0<1 pos feedback, <0 undefined
	float habituation;
	// signal multiplier from input after not firing:
	// >1 pos feedback, 0<1 neg feedback, <0 undefined
	float surprise;

	size_t inputSize;
	size_t outputSize;
	vector<float> neuronSignals;
	vector<bool> neuronStatuses;
	vector<bool> neuronQueueStatuses;
	queue<size_t> neuronQueue;
	Synapses synapses;

};
