#include "mind.h"
#include <stdexcept>

using namespace std;

Mind::Mind(
	size_t inputSizeParameter,
	size_t outputSizeParameter,
	const Synapses& synapsesParameter,
	float thresholdParameter,
	float memoryParameter,
	float forgetfulnessParameter,
	float habituationParameter,
	float surpriseParameter) :
	inputSize(inputSizeParameter),
	outputSize(outputSizeParameter),
	synapses(synapsesParameter),
	threshold(thresholdParameter),
	memory(memoryParameter),
	forgetfulness(forgetfulnessParameter),
	habituation(habituationParameter),
	surprise(surpriseParameter)
{
	size_t maxNeuron = inputSize + outputSize - 1;
	for (Synapses::const_iterator synapseIterator = synapses.cbegin(); synapseIterator != synapses.cend(); synapseIterator++)
	{
		if (synapseIterator->first > maxNeuron)
		{
			maxNeuron = synapseIterator->first;
		}
		else if (synapseIterator->second.first > maxNeuron)
		{
			maxNeuron = synapseIterator->second.first;
		}
	}

	neuronSignals.resize(maxNeuron + 1);
	neuronStatuses.resize(maxNeuron + 1);
	neuronQueueStatuses.resize(maxNeuron + 1);
}

Mind::Mind(const Mind& other) : 
	threshold(other.threshold),
	memory(other.memory),
	forgetfulness(other.forgetfulness),
	habituation(other.habituation),
	surprise(other.surprise),
	inputSize(other.inputSize),
	outputSize(other.outputSize),
	neuronSignals(other.neuronSignals),
	neuronStatuses(other.neuronStatuses),
	neuronQueueStatuses(other.neuronQueueStatuses),
	neuronQueue(other.neuronQueue),
	synapses(other.synapses)
{
}

Mind::Mind(Mind&& other) noexcept :
	threshold(other.threshold),
	memory(other.memory),
	forgetfulness(other.forgetfulness),
	habituation(other.habituation),
	surprise(other.surprise),
	inputSize(other.inputSize),
	outputSize(other.outputSize),
	neuronSignals(move(other.neuronSignals)),
	neuronStatuses(move(other.neuronStatuses)),
	neuronQueueStatuses(move(other.neuronQueueStatuses)),
	neuronQueue(move(other.neuronQueue)),
	synapses(move(other.synapses))
{
}

Mind& Mind::operator=(const Mind& other)
{
	threshold = other.threshold;
	memory = other.memory;
	forgetfulness = other.forgetfulness;
	habituation = other.habituation;
	surprise = other.surprise;
	inputSize = other.inputSize;
	outputSize = other.outputSize;
	neuronSignals = other.neuronSignals;
	neuronStatuses = other.neuronStatuses;
	neuronQueueStatuses = other.neuronQueueStatuses;
	neuronQueue = other.neuronQueue;
	synapses = other.synapses;

	return *this;
}

Mind& Mind::operator=(Mind&& other) noexcept
{
	threshold = other.threshold;
	memory = other.memory;
	forgetfulness = other.forgetfulness;
	habituation = other.habituation;
	surprise = other.surprise;
	inputSize = other.inputSize;
	outputSize = other.outputSize;
	neuronSignals = move(other.neuronSignals);
	neuronStatuses = move(other.neuronStatuses);
	neuronQueueStatuses = move(other.neuronQueueStatuses);
	neuronQueue = move(other.neuronQueue);
	synapses = move(other.synapses);

	return *this;
}

void Mind::Observe(const vector<bool>& input)
{
	if (input.size() != inputSize)
	{
		throw length_error("The given input does not match the input size for this Mind.");
	}

	for (size_t index = 0; index < inputSize; index++)
	{
		neuronSignals[index] = input[index] ? INFINITY : -INFINITY;
		neuronStatuses[index] = input[index];
		neuronQueue.push(index);
		neuronQueueStatuses[index] = true;
	}
}

vector<bool> Mind::Process(size_t fires)
{
	for (size_t fire = 0; fire < fires; fire++)
	{
		if (neuronQueue.empty())
		{
			return { false, false, false, false };
		}

		neuronStatuses[neuronQueue.front()] = (neuronSignals[neuronQueue.front()] >= threshold);
		neuronSignals[neuronQueue.front()] = 0.0f;

		pair<Synapses::iterator, Synapses::iterator> activeSynapses = synapses.equal_range(neuronQueue.front());
		neuronQueueStatuses[neuronQueue.front()] = false;
		neuronQueue.pop();
		
		for (Synapses::iterator synapseIterator = activeSynapses.first;
			synapseIterator != activeSynapses.second;
			synapseIterator++)
		{
			if (neuronStatuses[synapseIterator->first])
			{
				neuronSignals[synapseIterator->second.first] += synapseIterator->second.second;
				if (neuronQueueStatuses[synapseIterator->second.first] == false)
				{
					neuronQueue.push(synapseIterator->second.first);
					neuronQueueStatuses[synapseIterator->second.first] = true;
				}

				synapseIterator->second.second += memory;

				if (synapseIterator->first < inputSize)
				{
					synapseIterator->second.second /= habituation;
				}
			}
			else
			{
				synapseIterator->second.second -= forgetfulness;

				if (synapseIterator->first < inputSize)
				{
					synapseIterator->second.second *= surprise;
				}
			}
		}
	}

	vector<bool> output;
	for (size_t index = inputSize; index < inputSize + outputSize; index++)
	{
		output.push_back(neuronStatuses[index]);
	}
	return output;
}

Mind::Synapses Mind::GetSynapses() const
{
	return synapses;
}
