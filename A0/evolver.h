#pragma once

#include "mind.h"
#include <utility>
#include <random>

class Evolver
{
public:
	Evolver();

	Mind EvolveReflexMind(size_t generations,
		float initialMutationRate,
		size_t populationSize = 30,
		size_t survivorsSize = 1,
		size_t movesPerRound = 30,
		size_t roundsPerGeneration = 30);

private:
	random_device::result_type seed;
	minstd_rand rng;

	class ReflexAgent
	{
	public:
		ReflexAgent(
			pair<int, int> initialPosition,
			size_t inputSizeParameter,
			size_t outputSizeParameter,
			const Mind::Synapses& synapsesParameter,
			float thresholdParameter);

		const Mind& GetMind();

		pair<int, int> position;
		Mind mind;

	private:
		
	};

	Mind::Synapses Mutate(const Mind::Synapses& parentSynapses, float mutationRate);
};
