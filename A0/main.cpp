#include <iostream>
#include "mind.h"
#include <limits>
#include "evolver.h"

using namespace std;

int main()
{
	Evolver e;
	auto mind = e.EvolveReflexMind(100, 0.9f, 30, 1, 30, 30);
	auto synapses = mind.GetSynapses();

	for (auto i = synapses.cbegin(); i != synapses.cend(); i++)
	{
		cout << i->first << '\t' << i->second.first << '\t' << i->second.second << endl;
	}

	//Mind::Synapses graph;
	//graph.insert(make_pair(0, make_pair(8, 1.0f)));
	//graph.insert(make_pair(0, make_pair(9, 1.0f)));
	//graph.insert(make_pair(0, make_pair(10, 1.0f)));
	//graph.insert(make_pair(0, make_pair(11, 1.0f)));
	//graph.insert(make_pair(1, make_pair(8, 1.0f)));
	//graph.insert(make_pair(1, make_pair(9, 1.0f)));
	//graph.insert(make_pair(1, make_pair(10, 1.0f)));
	//graph.insert(make_pair(1, make_pair(11, 1.0f)));
	//graph.insert(make_pair(2, make_pair(8, 1.0f)));
	//graph.insert(make_pair(2, make_pair(9, 1.0f)));
	//graph.insert(make_pair(2, make_pair(10, 1.0f)));
	//graph.insert(make_pair(2, make_pair(11, 1.0f)));
	//graph.insert(make_pair(3, make_pair(8, 1.0f)));
	//graph.insert(make_pair(3, make_pair(9, 1.0f)));
	//graph.insert(make_pair(3, make_pair(10, 1.0f)));
	//graph.insert(make_pair(3, make_pair(11, 1.0f)));
	//graph.insert(make_pair(8, make_pair(4, 1.0f)));
	//graph.insert(make_pair(8, make_pair(5, 1.0f)));
	//graph.insert(make_pair(8, make_pair(6, 1.0f)));
	//graph.insert(make_pair(8, make_pair(7, 1.0f)));
	//graph.insert(make_pair(9, make_pair(4, 1.0f)));
	//graph.insert(make_pair(9, make_pair(5, 1.0f)));
	//graph.insert(make_pair(9, make_pair(6, 1.0f)));
	//graph.insert(make_pair(9, make_pair(7, 1.0f)));
	//graph.insert(make_pair(10, make_pair(4, 1.0f)));
	//graph.insert(make_pair(10, make_pair(5, 1.0f)));
	//graph.insert(make_pair(10, make_pair(6, 1.0f)));
	//graph.insert(make_pair(10, make_pair(7, 1.0f)));
	//graph.insert(make_pair(11, make_pair(4, 1.0f)));
	//graph.insert(make_pair(11, make_pair(5, 1.0f)));
	//graph.insert(make_pair(11, make_pair(6, 1.0f)));
	//graph.insert(make_pair(11, make_pair(7, 1.0f)));

	//Mind reflex(4, 4, graph, 1.0f);

	//reflex.Observe({ true, false, false, false });

	//for (int i = 0; i < 12; i++)
	//{
	//	auto out = reflex.Process();
	//	cout << out[0] << out[1] << out[2] << out[3] << endl;
	//}

	//auto out = reflex.Process(8);
	//cout << out[0] << out[1] << out[2] << out[3] << endl;

	system("pause");
	return EXIT_SUCCESS;
}
