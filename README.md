# Mindlab

## A0

Inspired by neural nets.
Prototype nonlinear neural net called neural mesh.
Tried to evolve weights in mesh to solve problems.
Stopped work because too difficult to represent neural mesh data in linear chromosome.

### Language

C++17

### IDE

Microsoft Visual Studio 2019

## A1

Inspired by Turing machine (computer).
Evolving programs in machine language.

### Language

C++17

### IDE

Microsoft Visual Studio 2019